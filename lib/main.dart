import 'package:flutter/material.dart';
import 'ui/application.dart';
import 'store/app/app_state.dart';
import 'package:redux/redux.dart';

void main() {
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
  );

  runApp(Application(store: store));
}