import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:reduxcounterapp/store/app/app_state.dart';
import 'package:reduxcounterapp/store/selectors.dart';

class MainDrawerViewModel {
  final void Function() clear;
  final void Function() changeColor;
  final bool selectColor;

  MainDrawerViewModel({
    this.clear,

    this.changeColor,
    this.selectColor,
  });

  factory MainDrawerViewModel.initState(Store<AppState> store) => MainDrawerViewModel(
        clear: CounterSelector.getClearFunction(store),
        changeColor: CounterSelector.changeColorFunction(store),
        selectColor: CounterSelector.getSelectColor(store),
      );
}
