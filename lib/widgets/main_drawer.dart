import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:reduxcounterapp/store/app/app_state.dart';
import 'main_drawer_view_model.dart';

class MainDrawer extends StatefulWidget {
  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, MainDrawerViewModel>(
      converter: (store) => MainDrawerViewModel.initState(store),
      builder: (context, viewModel) {
        return Drawer(
          child: Column(
            children: <Widget>[
              AppBar(
                automaticallyImplyLeading: false,
              ),
              RaisedButton(
                padding: EdgeInsets.all(20),
                child: Text(
                  'Clear value',
                  style: TextStyle(
                    fontSize: 30.0,
                  ),
                ),
                onPressed: () {
                  viewModel.clear();
                  Navigator.of(context).pop();
                },
              ),
              Switch(
                value: viewModel.selectColor,
                onChanged: (_) => viewModel.changeColor(),
              )
            ],
          ),
        );
      },
    );
  }
}
