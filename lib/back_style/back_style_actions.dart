import 'package:flutter/material.dart';

class ChangeColor{
  final Color color;

  ChangeColor(this.color);
}

class ChangeState{
  final bool selectBool;

  ChangeState(this.selectBool);
}