import 'package:flutter/material.dart';

import 'back_style_actions.dart';

class BackStyleState {
  final Color color;
  final bool selectColor;

  BackStyleState(
    this.color,
    this.selectColor,
  );

  factory BackStyleState.initial() => BackStyleState(Color(0xFF2b2a2a), false);

  BackStyleState reducer(dynamic action) {
    if (action == ChangeColor) {
      return BackStyleState(!selectColor ? Colors.orange : Color(0xFF2b2a2a),!selectColor);
    }
    return BackStyleState(color, selectColor);
  }
}
