import 'package:flutter/foundation.dart';
import 'package:reduxcounterapp/back_style/back_style_state.dart';
import 'package:reduxcounterapp/store/counter/counter_state.dart';

class AppState {
  final CounterState counterState;
  final BackStyleState backStyleState;

  AppState({
    @required this.counterState,
    @required this.backStyleState,
  });

  factory AppState.initial() {
    return AppState(
      counterState: CounterState.initial(),
      backStyleState: BackStyleState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';

    print('$TAG => <appReducer> => action: ${action}');

    return AppState(
      counterState: state.counterState.reducer(action),
      backStyleState: state.backStyleState.reducer(action),
    );
  }
}
