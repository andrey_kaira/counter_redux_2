import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:reduxcounterapp/back_style/back_style_actions.dart';
import 'package:reduxcounterapp/store/app/app_state.dart';
import 'counter/counter_actions.dart';

class CounterSelector{
  static void Function() getIncrementFunction(Store<AppState> store){
    return ()=> store.dispatch(Increment);
  }

  static void Function() getDecrementFunction(Store<AppState> store){
    return ()=> store.dispatch(Decrement);
  }

  static void Function() getClearFunction(Store<AppState> store){
    return ()=> store.dispatch(ClearAll);
  }

  static void Function() changeColorFunction(Store<AppState> store){
    return ()=> store.dispatch(ChangeColor);
  }

  static int getCurrentCounter(Store<AppState> store){
    return store.state.counterState.counter;
  }
  static bool getSelectColor(Store<AppState> store){
    return store.state.backStyleState.selectColor;
  }
  static Color getColor(Store<AppState> store){
    return store.state.backStyleState.color;
  }
}