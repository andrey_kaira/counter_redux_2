import 'package:flutter/cupertino.dart';

class Increment{
  final int count;

  Increment(this.count);
}

class Decrement{
  final int count;

  Decrement(this.count);
}

class ClearAll{
  final int count;

  ClearAll(this.count);
}