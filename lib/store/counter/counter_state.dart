import 'package:flutter/material.dart';

import 'counter_actions.dart';

class CounterState {
  final int counter;

  CounterState(this.counter);

  factory CounterState.initial() => CounterState(0);

  CounterState reducer(dynamic action) {
    if (action == Increment) {
      return CounterState(counter + 1);
    }
    if (action == Decrement) {
      return CounterState(counter - 1);
    }
    if (action == ClearAll) {
      return CounterState(0);
    }
    return CounterState(counter);
  }
}
