import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:reduxcounterapp/store/app/app_state.dart';
import 'package:reduxcounterapp/store/selectors.dart';

class MyAppViewModel {
  final void Function() increment;
  final void Function() decrement;
  final void Function() clear;
  final Color color;
  final int counter;

  MyAppViewModel({
    this.counter,
    this.color,
    this.decrement,
    this.increment,
    this.clear,
  });

  factory MyAppViewModel.initState(Store<AppState> store) => MyAppViewModel(
        increment: CounterSelector.getIncrementFunction(store),
        decrement: CounterSelector.getDecrementFunction(store),
        counter: CounterSelector.getCurrentCounter(store),
        clear: CounterSelector.getClearFunction(store),
        color: CounterSelector.getColor(store),
      );
}
