import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:reduxcounterapp/store/app/app_state.dart';
import 'my_app.dart';

class Application extends StatefulWidget {
  final Store<AppState> store;

  Application({Key key, @required this.store}) : super(key: key);

  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: StoreProvider(
        store: Store<AppState>(
          AppState.getAppReducer,
          initialState: AppState.initial(),
        ),
        child: MyApp(),
      ),
      theme: ThemeData.dark(),
    );
  }
}
