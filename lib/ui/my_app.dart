import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';
import 'package:reduxcounterapp/store/app/app_state.dart';
import 'package:reduxcounterapp/widgets/main_drawer.dart';
import 'my_app_view_model.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, MyAppViewModel>(
        converter: (store) => MyAppViewModel.initState(store),
        builder: (context, counter) {
          return Scaffold(
            backgroundColor: counter.color,
            appBar: AppBar(
              title: Text('Test'),
            ),
            drawer: MainDrawer(),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      child: Text('+'),
                      onPressed: counter.increment,
                    ),
                    Text(
                      '${counter.counter}',
                      style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                    ),
                    RaisedButton(
                      child: Text('-'),
                      onPressed: counter.decrement,
                    ),
                  ],
                )
              ],
            ),
          );
        },
    );
  }
}
